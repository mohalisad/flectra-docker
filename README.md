> **To add your pip packages edit the last line of Dockerfile**


**To create your own image run**
```
./helper build $YOUR_PREFERED_NAME
```


**To run flectra container alongside a postgres db ($YOUR_PREFERED_NAME must be the same as the build command)**
```
./helper up $SERVER_PORT $YOUR_PREFERED_NAME
```

**To close flectra containe and db**
```
./helper down
```

**To clean all the db and caches run**
```
./helper clean
```